RevenueClaiming
'''''''''''''''''''''''

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autosolcontract:: RevenueClaiming
    :noindex:
    :members: claimedAmounts, revenueAmount, revenueClaimed, claimers, constructor, setClaimed, checkClaim, getClaimedAmount, getRevenueAmount, getTotalClaimedAmount, getClaimersNumber, getClaimerAddress