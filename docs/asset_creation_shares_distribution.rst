Asset creation and shares distribution
================================================

The asset creation is voted via :code:`VotingApp` `Aragon`_ framework and is handled by :code:`AssetManager` smart contract.
In the following we assume the vote decision was to create a new asset, i.e. the community majority voted in favour of the creation.

In order to create a new asset, :code:`AssetManager` deploys two new smart contracts via :code:`createAsset()` function,
which are described below:

- an :code:`Asset` containing information about the asset and functions for the interaction with it (e.g. the revenue claim)
- a :code:`SHT`, a standard ERC20 token, used to handle the asset shares

More precisely, each **Asset** is defined by the following elements:

- a numerical **id**
- a **name**
- a **description**
- a **manager** address, the wallet of a trusted oracle, the unique allowed to set the revenues
- a **RVT** address, related to an ERC20 token used to distribute the revenues
- a **state**, with three available cases: :code:`ACTIVE` (default at the creation), :code:`INACTIVE` and :code:`FROZEN`

After the creation, 1000 :code:`SHT` are minted and initially assigned to the asset just created.
Then, the shares distribution is performed by community via :code:`VotingApp`
using :code:`distributeShares()` function.

Regarding the asset available states, their explanations are reported in the following list:

- :code:`state=ACTIVE`: shares can be exchanged, instead no revenues cannot be claimed
- :code:`state=FROZEN`: shares cannot be exchanged, revenues can be claimed by share owners
- :code:`state=INACTIVE`: shares cannot be exchanged and revenues cannot be claimed

The state transitions, shown in the figure below, are handled by :code:`activateAsset()`, :code:`deactivateAsset()`,
:code:`setRevenue()` and :code:`defrostAsset()` functions,
which will be explained in the next chapters.

.. image:: _static/states_transitions.png
    :scale: 100%
    :align: center

.. _Aragon: https://aragon.org/
.. _VotingApp: https://github.com/aragon/aragon-apps/tree/master/apps/voting



