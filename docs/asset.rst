Asset
'''''''''''''''''''''''

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autosolcontract:: Asset
    :noindex:
    :members: ClaimExecuted, features, activeRevenueClaiming, sht, rvt, revenueClaimings, constructor, setRevenue, defrost, activate, deactivate, claim, distributeShares, getState, getName, getManager, getDescription, getSHTAddress, getRVTAddress, getRevenueClaimings, getActiveRevenueClaiming