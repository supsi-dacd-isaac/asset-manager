How it works
==================

The main aim of these smart contracts is to provide a blockhain-based asset management.
Practically, an asset is defined as an element with the following main features:

- its ownerships is handled via a shares mechanism
- it is able to generate revenues, which can be redistributed to the share owners

The asset management is thought to be integrated inside an `Aragon`_ platform in order to use the functionalities already provided.
Consequently, an Aragon community can have more assets. Each asset has its own shares holders.

In general, the most significant operations needed in the asset handling have to be managed by the `VotingApp`_ provided by Aragon.
Instead in other cases this approach is not required/convenient.

Below the operations handled via :code:`VotingApp` application are reported:

- **asset creation**
- **share distribution**, initially owned by the asset itself
- **asset deactivation** (i.e. the asset has problems and needs to be repaired before restarting to generate revenues)
- **asset activation**, to activate the asset after a deactivation

As it can be easily understood, the aforementioned operations are extremely significant. Indeed, the related blockchain transactions
occur few times during an asset life.
In order to properly consider their importance, the transactions effective running has to be voted by the community via :code:`VotingApp`.

On the other hand, other operations do not need to be voted by the entire community, they are reported below:

- **revenue setting**, performed by a trusted community manager
- **revenue claims**, performed by share owners

In the next chapters, the aforementioned operations and the related smart contracts will be explained in details.

.. toctree::
    :maxdepth: 2

    asset_creation_shares_distribution
    revenue_setting_claims_defrosting
    deactivation_activation

.. _Aragon: https://aragon.org/
.. _VotingApp: https://github.com/aragon/aragon-apps/tree/master/apps/voting
