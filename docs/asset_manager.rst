AssetManager
'''''''''''''''''''''''

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autosolcontract:: AssetManager
    :noindex:
    :members: AssetCreated, AssetDeactivated, AssetActivated, AssetFrozen, AssetDefrosted, SharesDistributed, RevenueSet, assetsList, CREATE_ASSET_ROLE, DEACTIVATE_ASSET_ROLE, ACTIVATE_ASSET_ROLE, DISTRIBUTE_SHARES_ROLE, initialize, createAsset, activateAsset, deactivateAsset, distributeShares, setRevenue, defrostAsset, getSHTAddress, getManager, getAssetAddress