Revenue setting, claims and defrosting
================================================

When the asset state is :code:`ACTIVE` a revenue setting can be performed by the asset :code:`manager` using :code:`setRevenue()` function.
After this operation, the asset becomes :code:`FROZEN` and a certain amount of :code:`RVT` tokens are staked in the :code:`Asset` instance.
Besides, a new :code:`RevenueClaiming` contract is deployed; it contains all the information related to the revenue and
its claims (e.g. the claimed amount, the claimers list, etc.).

When the state is :code:`FROZEN`, the shares cannot be transferred in order to avoid double-spending cheatings.
Indeed, each share owner is allowed to claim its revenue portion, i.e. a part of :code:`RVT` staking proportional to owned shares, via :code:`claim()` function.

After a certain amount of time, reasonably long to assure an easy claim to the share owners, the asset is defrosted via :code:`defrostAsset()` function.
As for :code:`setRevenue()`, asset :code:`manager` is the unique address allowed to perform :code:`defrostAsset()`.

The following image shows the timeline of a revenue "life": the initial setting, the claims when the asset is frozen, and the defrosting.

.. image:: _static/revenue_sequence.png
    :scale: 100%
    :align: center

When an asset is :code:`FROZEN`, no new revenue can be set, i.e. the life of revenue currently claimable has to end with a defrosting
before having a new setting.