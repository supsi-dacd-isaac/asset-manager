Deactivation and activation
=============================

When an :code:`Asset` is unable to create new revenues (e.g. it has a problem and needs to be repaired, or
it will never generate revenues anymore being definitely broken) the community, voting via :code:`VotingApp`,
can decide to set the :code:`Asset` state to :code:`INACTIVE` using the function :code:`deactivateAsset()`.

When state is :code:`INACTIVE`, the asset shares are locked and no revenues can be set.
In case an :code:`Asset` is able again to generate new revenues (i.e. a broken asset has been repaired),
the community, via :code:`VotingApp`, can reactivate it using the function :code:`activateAsset()`.
