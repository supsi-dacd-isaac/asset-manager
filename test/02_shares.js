/* global artifacts contract beforeEach it assert */

const { assertRevert } = require('@aragon/test-helpers/assertThrow')
const { hash } = require('eth-ens-namehash')

const { assertAmountOfEvents } = require('@aragon/test-helpers/assertEvent')(web3)
const { getEventAt, getEventArgument, getNewProxyAddress } = require('@aragon/test-helpers/events')
const getBlockNumber = require('@aragon/test-helpers/blockNumber')(web3)
const { encodeCallScript, EMPTY_SCRIPT } = require('@aragon/test-helpers/evmScript')
const { makeErrorMappingProxy } = require('@aragon/test-helpers/utils')
// const ExecutionTarget = artifacts.require('ExecutionTarget')

const deployDAO = require('./helpers/deployDAO');
const constants = require('./helpers/Constants');

const AssetManager = artifacts.require('AssetManager.sol');
const Asset = artifacts.require('Asset.sol');
const SHT = artifacts.require('SHT');
const RVT = artifacts.require('RVT');

contract('AssetManager - Shares', ([appManager, chief, manager, user1, user2, cheater]) => {

  // Main variables
  let app, addrSHT, sht, addrAsset, asset, receivers = [user1, user2], rvt;

  beforeEach('deploy dao and app', async () => {
    const { dao, acl } = await deployDAO(appManager)

    // Deploy the app's base contract.
    const appBase = await AssetManager.new();

    // Deploy the RVT instance
    rvt = await RVT.new();

    // Instantiate a proxy for the app, using the base contract as its logic implementation.
    const instanceReceipt = await dao.newAppInstance(
      hash('asset-manager.aragonpm.test'), // appId - Unique identifier for each app installed in the DAO; can be any bytes32 string in the tests.
      appBase.address, // appBase - Location of the app's base implementation.
      '0x', // initializePayload - Used to instantiate and initialize the proxy in the same call (if given a non-empty bytes string).
      false, // setDefault - Whether the app proxy is the default proxy.
      { from: appManager }
    )

    // Define the Asset manager instance
    app = AssetManager.at(getEventArgument(instanceReceipt, 'NewAppProxy', 'proxy'))

    // Set up the app's permissions.
    // Example based on CounterApp
    // await acl.createPermission(
    //   constants.ANY_ADDRESS, // entity (who?) - The entity or address that will have the permission.
    //   app.address, // app (where?) - The app that holds the role involved in this permission.
    //   await app.INCREMENT_ROLE(), // role (what?) - The particular role that the entity is being assigned to in this permission.
    //   appManager, // manager - Can grant/revoke further permissions for this role.
    //   { from: appManager }
    // )

    // Only the chief can add an asset
    await acl.createPermission(chief, app.address, await app.CREATE_ASSET_ROLE(), appManager, { from: appManager });

    // Only the chief can deactivate an asset
    await acl.createPermission(chief, app.address, await app.DEACTIVATE_ASSET_ROLE(), appManager, { from: appManager });

    // Only the chief can distribute shares
    await acl.createPermission(chief, app.address, await app.DISTRIBUTE_SHARES_ROLE(), appManager, { from: appManager });

    await app.initialize();

    // Create a new asset instance
    await app.createAsset(constants.NEW_ASSET.ID, constants.NEW_ASSET.NAME, constants.NEW_ASSET.DESCRIPTION, manager, rvt.address, { from: chief })
  });

  describe('Successful shares assignments:', function() {
    it('Balances checking', async () => {
      // Get SHT instance
      addrSHT = await app.getSHTAddress(constants.NEW_ASSET.ID);
      sht = await SHT.at(addrSHT);

      // Distribute shares
      await app.distributeShares(constants.NEW_ASSET.ID, receivers, constants.SHARES_DISTRIBUTION, { from: chief });

      // Check the balances of user1/user2
      let total = 0;
      for (let i=0; i < receivers.length; i++) {
        assert.equal((await sht.balanceOf(receivers[i])).c[0], constants.SHARES_DISTRIBUTION[i]);
        total += constants.SHARES_DISTRIBUTION[i];
      }

      // Check the balance of 0x00 address (it must be always 0)
      assert.equal((await sht.balanceOf(constants.ZERO_ADDRESS)).c[0], 0);

      // Check the asset balance after the distribution
      assert.equal((await sht.balanceOf(await app.getAssetAddress(constants.NEW_ASSET.ID))).c[0], constants.SHARES_AMOUNT-total);
    })
  });

  describe('Failed shares assignments:', function() {
    it('Cheater without permissions trying to assign shares', async () => {
      // Try to distribute shares
      await assertRevert(app.distributeShares(constants.NEW_ASSET.ID, receivers, constants.SHARES_DISTRIBUTION, { from: cheater }));
    });

    it('Different numbers of receivers and amounts', async () => {
      // Try to distribute shares
      await assertRevert(app.distributeShares(constants.NEW_ASSET.ID, [user1], constants.SHARES_DISTRIBUTION, { from: chief }));
    });

    it('Too many shares to distribute', async () => {
      // Try to distribute shares
      await assertRevert(app.distributeShares(constants.NEW_ASSET.ID, [user1], [constants.SHARES_AMOUNT+1], { from: chief }));
    });

    it('Distribution when asset is inactive', async () => {
      // Deactivate the asset
      await app.deactivateAsset(constants.NEW_ASSET.ID, { from: chief });

      // Try to distribute shares
      await assertRevert(app.distributeShares(constants.NEW_ASSET.ID, [constants.ZERO_ADDRESS], [1], { from: chief }));
    });

    it('Distribution when asset is frozen', async () => {
      // Get Asset instance
      let addrAsset = await app.getAssetAddress(constants.NEW_ASSET.ID);
      let asset = await Asset.at(addrAsset);

      // Distribute shares
      await app.distributeShares(constants.NEW_ASSET.ID, receivers, constants.SHARES_DISTRIBUTION, { from: chief });

      // Mint token and set properly the allowance to the asset
      rvt.mint(manager, constants.RVT_INITIAL_AMOUNT);
      rvt.approve(asset.address, constants.RVT_INITIAL_AMOUNT, { from: manager });
      assert.equal(await rvt.allowance(manager, asset.address), constants.RVT_INITIAL_AMOUNT);

      // Set the revenue
      await app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: manager });

      // Try to distribute shares
      await assertRevert(app.distributeShares(constants.NEW_ASSET.ID, [constants.ZERO_ADDRESS], [1], { from: chief }));
    });
  });
});
