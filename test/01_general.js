/* global artifacts contract beforeEach it assert */

const { assertRevert } = require('@aragon/test-helpers/assertThrow')
const { hash } = require('eth-ens-namehash')

const { assertAmountOfEvents } = require('@aragon/test-helpers/assertEvent')(web3)
const { getEventAt, getEventArgument, getNewProxyAddress } = require('@aragon/test-helpers/events')
const getBlockNumber = require('@aragon/test-helpers/blockNumber')(web3)
const { encodeCallScript, EMPTY_SCRIPT } = require('@aragon/test-helpers/evmScript')
const { makeErrorMappingProxy } = require('@aragon/test-helpers/utils')
// const ExecutionTarget = artifacts.require('ExecutionTarget')

const deployDAO = require('./helpers/deployDAO');
const constants = require('./helpers/Constants');

const AssetManager = artifacts.require('AssetManager.sol');
const Asset = artifacts.require('Asset.sol');
const SHT = artifacts.require('SHT');
const RVT = artifacts.require('RVT');

contract('AssetManager - Assets creation/deactivation/activation', ([appManager, chief, user, manager, cheater]) => {

  // Main variables
  let app, addrSHT, sht, addrAsset, asset, rvt;

  beforeEach('deploy dao and app', async () => {
    const { dao, acl } = await deployDAO(appManager)

    // Deploy the app's base contract.
    const appBase = await AssetManager.new();

    // Deploy the RVT instance
    rvt = await RVT.new();

    // Instantiate a proxy for the app, using the base contract as its logic implementation.
    const instanceReceipt = await dao.newAppInstance(
      hash('asset-manager.aragonpm.test'), // appId - Unique identifier for each app installed in the DAO; can be any bytes32 string in the tests.
      appBase.address, // appBase - Location of the app's base implementation.
      '0x', // initializePayload - Used to instantiate and initialize the proxy in the same call (if given a non-empty bytes string).
      false, // setDefault - Whether the app proxy is the default proxy.
      { from: appManager }
    )

    // Define the Asset manager instance
    app = AssetManager.at(getEventArgument(instanceReceipt, 'NewAppProxy', 'proxy'))

    // Set up the app's permissions.
    // Example based on CounterApp
    // await acl.createPermission(
    //   constants.ANY_ADDRESS, // entity (who?) - The entity or address that will have the permission.
    //   app.address, // app (where?) - The app that holds the role involved in this permission.
    //   await app.INCREMENT_ROLE(), // role (what?) - The particular role that the entity is being assigned to in this permission.
    //   appManager, // manager - Can grant/revoke further permissions for this role.
    //   { from: appManager }
    // )

    // Only the chief can create a new asset
    await acl.createPermission(chief, app.address, await app.CREATE_ASSET_ROLE(), appManager, { from: appManager });

    // Only the chief can deactivate an asset
    await acl.createPermission(chief, app.address, await app.DEACTIVATE_ASSET_ROLE(), appManager, { from: appManager });

    // Only the chief can activate an asset
    await acl.createPermission(chief, app.address, await app.ACTIVATE_ASSET_ROLE(), appManager, { from: appManager });

    await app.initialize();

      // Create a new asset instance
      await app.createAsset(constants.NEW_ASSET.ID, constants.NEW_ASSET.NAME, constants.NEW_ASSET.DESCRIPTION, manager,
                            rvt.address, { from: chief });
  });

  describe('Asset creation:', function() {
    it('Successful asset creation', async () => {
      // Get Asset instance
      addrAsset = await app.getAssetAddress(constants.NEW_ASSET.ID);
      asset = await Asset.at(addrAsset);

      // Get SHT instance
      addrSHT = await app.getSHTAddress(constants.NEW_ASSET.ID);
      sht = await SHT.at(addrSHT);

      // Check existence/state of the asset and its features
      assert.equal(await asset.getState({ from: user }), constants.ASSET_STATES.ACTIVE);
      assert.equal(await asset.getName({ from: user }), constants.NEW_ASSET.NAME);
      assert.equal(await sht.getAssetAdddress({ from: user }), addrAsset);

      // Check the token minting and balance
      assert.equal((await sht.totalSupply()).c[0], constants.SHARES_AMOUNT);
      assert.equal((await sht.balanceOf(await app.getAssetAddress(constants.NEW_ASSET.ID))).c[0], constants.SHARES_AMOUNT);
    });

    it('Failed asset creation (cheater trying to perform transaction)', async () => {
      // Try to create a new asset without authorization
      await assertRevert(
        app.createAsset(constants.NEW_ASSET.ID+1, constants.NEW_ASSET.NAME, constants.NEW_ASSET.DESCRIPTION,
                     manager, rvt.address, { from: cheater }), constants.ERRORS.APP_AUTH_FAILED)
    });
  });

  describe('Successful deactivation:', function() {
    it('Successful asset deactivation', async () => {
      // Get Asset instance
      addrAsset = await app.getAssetAddress(constants.NEW_ASSET.ID);
      asset = await Asset.at(addrAsset);

      // Check existence/state of the asset and its features
      assert.equal(await asset.getState({from: user }), constants.ASSET_STATES.ACTIVE);

      // Deactivate the asset
      await app.deactivateAsset(constants.NEW_ASSET.ID, { from: chief });

      // Check existence/state of the asset and its features
      assert.equal(await asset.getState({ from: user }), constants.ASSET_STATES.INACTIVE)
    })

    it('Failed asset deactivations (cheater trying to perform transaction)', async () => {
      // Try to deactivate an asset without authorization
      await assertRevert(app.deactivateAsset(constants.NEW_ASSET.ID, { from: cheater }));

      // Deactivate the asset
      await app.deactivateAsset(constants.NEW_ASSET.ID, { from: chief });

      // Try to deactivate again the asset
      await assertRevert(app.deactivateAsset(constants.NEW_ASSET.ID, { from: chief }));
    });
  });

  describe('Asset activation:', function() {
    it('Successful asset activation', async () => {
      // Get Asset instance
      addrAsset = await app.getAssetAddress(constants.NEW_ASSET.ID);
      asset = await Asset.at(addrAsset);

      // Check the asset state
      assert.equal(await asset.getState({ from: user }), constants.ASSET_STATES.ACTIVE);

      // Deactivate the asset just created
      await app.deactivateAsset(constants.NEW_ASSET.ID, { from: chief });

      // Check the asset state
      assert.equal(await asset.getState({ from: user }), constants.ASSET_STATES.INACTIVE);

      // Re-activate the asset just frozen
      await app.activateAsset(constants.NEW_ASSET.ID, { from: chief });

      // Check the asset state
      assert.equal(await asset.getState({ from: user }), constants.ASSET_STATES.ACTIVE);
    });

    it('Failed asset activation (asset already active)', async () => {
      // Re-activate the asset just frozen
      await assertRevert(app.activateAsset(constants.NEW_ASSET.ID, { from: chief }));
    });

    it('Failed asset activation (cheater trying to perform transaction)', async () => {
      // Deactivate the asset just created
      await app.deactivateAsset(constants.NEW_ASSET.ID, { from: chief });

      // Re-activate the asset just frozen
      await assertRevert(app.activateAsset(constants.NEW_ASSET.ID, { from: cheater }));
    })
  });
});
