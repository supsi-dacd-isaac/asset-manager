/* global artifacts contract beforeEach it assert */

const { assertRevert } = require('@aragon/test-helpers/assertThrow')
const { hash } = require('eth-ens-namehash')

const { assertAmountOfEvents } = require('@aragon/test-helpers/assertEvent')(web3)
const { getEventAt, getEventArgument, getNewProxyAddress } = require('@aragon/test-helpers/events')
const getBlockNumber = require('@aragon/test-helpers/blockNumber')(web3)
const { encodeCallScript, EMPTY_SCRIPT } = require('@aragon/test-helpers/evmScript')
const { makeErrorMappingProxy } = require('@aragon/test-helpers/utils')
// const ExecutionTarget = artifacts.require('ExecutionTarget')

const deployDAO = require('./helpers/deployDAO');
const constants = require('./helpers/Constants');

const AssetManager = artifacts.require('AssetManager.sol');
const Asset = artifacts.require('Asset.sol');
const RevenueClaiming = artifacts.require('RevenueClaiming.sol');
const SHT = artifacts.require('SHT');
const RVT = artifacts.require('RVT');

contract('AssetManager - Successful revenues/claims/defrostings', ([appManager, chief, communityManager, user1, user2, cheater]) => {

  // Main variables
  let app, addrSHT, sht, addrAsset, asset, receivers = [user1, user2], rvt, revenueClaim, addrRC;
  let claimedAmountUser1, claimedAmountUser2, tmpAmount, assetAmount;

  beforeEach('deploy dao and app', async () => {
    const { dao, acl } = await deployDAO(appManager)

    // Deploy the app's base contract.
    const appBase = await AssetManager.new();

    // Deploy the RVT instance
    rvt = await RVT.new();
    rvt.mint(communityManager, constants.RVT_INITIAL_AMOUNT);

    // Instantiate a proxy for the app, using the base contract as its logic implementation.
    const instanceReceipt = await dao.newAppInstance(
      hash('asset-manager.aragonpm.test'), // appId - Unique identifier for each app installed in the DAO; can be any bytes32 string in the tests.
      appBase.address, // appBase - Location of the app's base implementation.
      '0x', // initializePayload - Used to instantiate and initialize the proxy in the same call (if given a non-empty bytes string).
      false, // setDefault - Whether the app proxy is the default proxy.
      { from: appManager }
    )

    // Define the Asset manager instance
    app = AssetManager.at(getEventArgument(instanceReceipt, 'NewAppProxy', 'proxy'))

    // Set up the app's permissions.
    // Example based on CounterApp
    // await acl.createPermission(
    //   constants.ANY_ADDRESS, // entity (who?) - The entity or address that will have the permission.
    //   app.address, // app (where?) - The app that holds the role involved in this permission.
    //   await app.INCREMENT_ROLE(), // role (what?) - The particular role that the entity is being assigned to in this permission.
    //   appManager, // manager - Can grant/revoke further permissions for this role.
    //   { from: appManager }
    // )

    // Only the chief can add an asset
    await acl.createPermission(chief, app.address, await app.CREATE_ASSET_ROLE(), appManager, { from: appManager });

    // Only the chief can deactivate an asset
    await acl.createPermission(chief, app.address, await app.DEACTIVATE_ASSET_ROLE(), appManager, { from: appManager });

    // Only the chief can distribute shares
    await acl.createPermission(chief, app.address, await app.DISTRIBUTE_SHARES_ROLE(), appManager, { from: appManager });

    await app.initialize()

    // Create a new asset instance
    await app.createAsset(constants.NEW_ASSET.ID, constants.NEW_ASSET.NAME, constants.NEW_ASSET.DESCRIPTION, communityManager, rvt.address, { from: chief })

    // Get Asset instance
    addrAsset = await app.getAssetAddress(constants.NEW_ASSET.ID);
    asset = await Asset.at(addrAsset);

    // Distribute shares
    await app.distributeShares(constants.NEW_ASSET.ID, receivers, constants.SHARES_DISTRIBUTION, { from: chief });

    // set the allowance to the asset address
    rvt.approve(asset.address, constants.RVT_INITIAL_AMOUNT, { from: communityManager });
    assert.equal(await rvt.allowance(communityManager, asset.address), constants.RVT_INITIAL_AMOUNT);

    // Set the revenue and freeze the asset
    await app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: communityManager });

    // Get SHT instance
    addrSHT = await app.getSHTAddress(constants.NEW_ASSET.ID);
    sht = await SHT.at(addrSHT);

    // Get RevenueClaim instance
    addrRC = await asset.getActiveRevenueClaiming();
    revenueClaim = await RevenueClaiming.at(addrRC);
  });

  describe('Single revenue assignment and related claim:', function() {
    it('Revenues assignment', async () => {
      // Check RVT balances and asset state
      assert.equal(await rvt.balanceOf(asset.address), constants.REVENUE_AMOUNT1);
      assert.equal(await rvt.balanceOf(communityManager), (constants.RVT_INITIAL_AMOUNT-constants.REVENUE_AMOUNT1));
      assert.equal(await asset.getState(), constants.ASSET_STATES.FROZEN);
      assert.equal(await revenueClaim.getRevenueAmount(), constants.REVENUE_AMOUNT1);
    });

    it('Completed claim', async () => {
      // Check before claim
      assert.equal(await rvt.balanceOf(user1), 0);
      assert.equal(await rvt.balanceOf(user2), 0);
      assert.equal(await revenueClaim.getTotalClaimedAmount(), 0);
      assert.equal(await revenueClaim.getClaimersNumber(), 0);

      // Claim performed by user1
      await asset.claim({ from: user1 });

      // Check after claim
      claimedAmountUser1 = constants.REVENUE_AMOUNT1*(await sht.balanceOf(user1))/(await sht.totalSupply());
      assert.equal(await rvt.balanceOf(user1), claimedAmountUser1);
      assert.equal(await revenueClaim.getClaimedAmount(user1), claimedAmountUser1);
      assert.equal(await revenueClaim.getTotalClaimedAmount(), claimedAmountUser1);
      assert.equal(await revenueClaim.getClaimersNumber(), 1);
      assert.equal(await revenueClaim.getClaimerAddress(0), user1);
      assert.equal(await revenueClaim.getClaimerAddress(1), constants.ZERO_ADDRESS);

      // Claim performed by user 2
      await asset.claim({ from: user2 });

      // Check after claim
      claimedAmountUser2 = constants.REVENUE_AMOUNT1*(await sht.balanceOf(user2))/(await sht.totalSupply());
      assert.equal(await rvt.balanceOf(user2), claimedAmountUser2);
      assert.equal(await revenueClaim.getClaimedAmount(user2), claimedAmountUser2);
      assert.equal(await revenueClaim.getTotalClaimedAmount(), claimedAmountUser1+claimedAmountUser2);
      assert.equal(await revenueClaim.getClaimersNumber(), 2);
      assert.equal(await revenueClaim.getClaimerAddress(1), user2);
      assert.equal(await revenueClaim.getClaimerAddress(2), constants.ZERO_ADDRESS);
    });
  });

  describe('Multiple revenue assignments/defrosting with related claims:', function() {
    it('Two revenues assignments, the first with a partial claim (i.e. some share owners will not claim their revenue part), the second complete', async () => {
      // Check before claim
      assert.equal(await rvt.balanceOf(user1), 0);
      assert.equal(await rvt.balanceOf(user2), 0);
      assert.equal(await revenueClaim.getTotalClaimedAmount(), 0);
      assert.equal(await revenueClaim.getClaimersNumber(), 0);

      // Claim performed by user1
      await asset.claim({ from: user1 });

      // Check after claim
      claimedAmountUser1 = constants.REVENUE_AMOUNT1*(await sht.balanceOf(user1))/(await sht.totalSupply());
      assert.equal(await rvt.balanceOf(user1), claimedAmountUser1);
      assert.equal(await revenueClaim.getClaimedAmount(user1), claimedAmountUser1);
      assert.equal(await revenueClaim.getTotalClaimedAmount(), claimedAmountUser1);
      assert.equal(await revenueClaim.getClaimersNumber(), 1);
      assert.equal(await revenueClaim.getClaimerAddress(0), user1);
      assert.equal(await revenueClaim.getClaimerAddress(1), constants.ZERO_ADDRESS);

      // check the RVT amount owned by the asset
      assetAmount = (await revenueClaim.getRevenueAmount()) - (await revenueClaim.getTotalClaimedAmount())
      assert.equal((await rvt.balanceOf(asset.address)).c[0], assetAmount);

      // Defrost the asset
      await app.defrostAsset(constants.NEW_ASSET.ID, { from: communityManager });

      // check the RVT amount owned by asset after the defrosting (it must be 0)
      assert.equal((await rvt.balanceOf(asset.address)).c[0], 0);

      // Check the asset state
      assert.equal(await asset.getState(), constants.ASSET_STATES.ACTIVE);

      // Set the revenue and freeze the asset
      await app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT2, { from: communityManager });

      // Get RevenueClaim instance
      addrRC = await asset.getActiveRevenueClaiming();
      revenueClaim = await RevenueClaiming.at(addrRC);

      tmpAmount = parseInt(await rvt.balanceOf(user1));

      // Claim performed by user1 (smart user, it always performs claim when there is a revenue)
      await asset.claim({ from: user1 });

      // Check after claim
      claimedAmountUser1 = constants.REVENUE_AMOUNT2*(await sht.balanceOf(user1))/(await sht.totalSupply());
      assert.equal((await rvt.balanceOf(user1)).c[0], tmpAmount+claimedAmountUser1);
      assert.equal(await revenueClaim.getClaimedAmount(user1), claimedAmountUser1);

      // Claim performed by user2 (lazy user, it has missed the previous claim)
      await asset.claim({ from: user2 });

      // Check after claim
      claimedAmountUser2 = constants.REVENUE_AMOUNT2*(await sht.balanceOf(user2))/(await sht.totalSupply());
      assert.equal((await rvt.balanceOf(user2)).c[0], claimedAmountUser2);
      assert.equal(await revenueClaim.getClaimedAmount(user2), claimedAmountUser2);
    });
  });
});
