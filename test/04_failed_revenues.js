/* global artifacts contract beforeEach it assert */

const { assertRevert } = require('@aragon/test-helpers/assertThrow')
const { hash } = require('eth-ens-namehash')

const { assertAmountOfEvents } = require('@aragon/test-helpers/assertEvent')(web3)
const { getEventAt, getEventArgument, getNewProxyAddress } = require('@aragon/test-helpers/events')
const getBlockNumber = require('@aragon/test-helpers/blockNumber')(web3)
const { encodeCallScript, EMPTY_SCRIPT } = require('@aragon/test-helpers/evmScript')
const { makeErrorMappingProxy } = require('@aragon/test-helpers/utils')
// const ExecutionTarget = artifacts.require('ExecutionTarget')

const deployDAO = require('./helpers/deployDAO');
const constants = require('./helpers/Constants');

const AssetManager = artifacts.require('AssetManager.sol');
const Asset = artifacts.require('Asset.sol');
const RevenueClaiming = artifacts.require('RevenueClaiming.sol');
const SHT = artifacts.require('SHT');
const RVT = artifacts.require('RVT');

contract('AssetManager - Failed revenues/claims/defrosting', ([appManager, chief, communityManager, user1, user2, cheater]) => {

  // Main variables
  let app, addrSHT, sht, addrAsset, asset, receivers = [user1, user2], rvt, revenueClaiming, addrRC, revenueClaim;
  let claimedAmountUser1, claimedAmountUser2, tmpAmount, assetAmount;

  beforeEach('deploy dao and app', async () => {
    const { dao, acl } = await deployDAO(appManager)

    // Deploy the app's base contract.
    const appBase = await AssetManager.new();

    // Deploy the RVT instance
    rvt = await RVT.new();
    rvt.mint(communityManager, constants.RVT_INITIAL_AMOUNT);

    // Instantiate a proxy for the app, using the base contract as its logic implementation.
    const instanceReceipt = await dao.newAppInstance(
      hash('asset-manager.aragonpm.test'), // appId - Unique identifier for each app installed in the DAO; can be any bytes32 string in the tests.
      appBase.address, // appBase - Location of the app's base implementation.
      '0x', // initializePayload - Used to instantiate and initialize the proxy in the same call (if given a non-empty bytes string).
      false, // setDefault - Whether the app proxy is the default proxy.
      { from: appManager }
    )

    // Define the Asset manager instance
    app = AssetManager.at(getEventArgument(instanceReceipt, 'NewAppProxy', 'proxy'))

    // Set up the app's permissions.
    // Example based on CounterApp
    // await acl.createPermission(
    //   constants.ANY_ADDRESS, // entity (who?) - The entity or address that will have the permission.
    //   app.address, // app (where?) - The app that holds the role involved in this permission.
    //   await app.INCREMENT_ROLE(), // role (what?) - The particular role that the entity is being assigned to in this permission.
    //   appManager, // manager - Can grant/revoke further permissions for this role.
    //   { from: appManager }
    // )

    // Only the chief can add an asset
    await acl.createPermission(chief, app.address, await app.CREATE_ASSET_ROLE(), appManager, { from: appManager });

    // Only the chief can deactivate an asset
    await acl.createPermission(chief, app.address, await app.DEACTIVATE_ASSET_ROLE(), appManager, { from: appManager });

    // Only the chief can distribute shares
    await acl.createPermission(chief, app.address, await app.DISTRIBUTE_SHARES_ROLE(), appManager, { from: appManager });

    await app.initialize()

    // Create a new asset instance
    await app.createAsset(constants.NEW_ASSET.ID, constants.NEW_ASSET.NAME, constants.NEW_ASSET.DESCRIPTION,
                          communityManager, rvt.address, { from: chief });

    // Get asset instance
    addrAsset = await app.getAssetAddress(constants.NEW_ASSET.ID);
    asset = await Asset.at(addrAsset);

    // Distribute shares
    await app.distributeShares(constants.NEW_ASSET.ID, receivers, constants.SHARES_DISTRIBUTION, { from: chief });

    // Set the allowance to the asset address for the revenues tokens
    rvt.approve(asset.address, constants.RVT_INITIAL_AMOUNT, { from: communityManager });
    assert.equal(await rvt.allowance(communityManager, asset.address), constants.RVT_INITIAL_AMOUNT);
  });

  describe('Failed revenues assignments:', function() {
    it('A cheater (i.e. not the community manager) trying to set a revenue', async () => {
      // Try to set the revenue
      await assertRevert(app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: cheater }));
    });

    it('Trying to set a revenue when the asset is inactive', async () => {
      // Deactivate the asset
      await app.deactivateAsset(constants.NEW_ASSET.ID, { from: chief });

      // Try to set the revenue
      await assertRevert(app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: communityManager }));
    });

    it('Trying to set a revenue when the asset is frozen', async () => {
      // Set the revenue
      await app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: communityManager });

      // Try to set the revenue again
      await assertRevert(app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: communityManager }));
    });

    it('Trying to perform a claim when the asset is active', async () => {
      // Try to perform a claim
      await assertRevert(asset.claim({ from: user1 }));
    });

    it('Trying to perform a claim when the asset is inactive', async () => {
      // Deactivate the asset
      await app.deactivateAsset(constants.NEW_ASSET.ID, { from: chief });

      // Trying to perform a claim
      await assertRevert(asset.claim({ from: user1 }));
    });

    it('A cheater without shares trying to perform a claim', async () => {
      // Set the revenue
      await app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: communityManager });

      // Trying to perform a claim
      await assertRevert(asset.claim({ from: cheater }));
    });

    it('A cheater shares owner performing more claims', async () => {
      // Set the revenue
      await app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: communityManager });

      // Claiming performed by user1
      await asset.claim({ from: user1 });

      // user1 trying to perform a claim again
      await assertRevert(asset.claim({ from: user1 }));
    });
  });

  describe('Failed defrostings:', function() {
      it('A cheater trying to defrost an asset', async () => {
      await app.setRevenue(constants.NEW_ASSET.ID, constants.REVENUE_AMOUNT1, { from: communityManager });

      await assertRevert(app.defrostAsset(constants.NEW_ASSET.ID, { from: cheater }));
    });

    it('Trying to defrost a not frozen asset', async () => {
      await assertRevert(app.defrostAsset(constants.NEW_ASSET.ID, { from: communityManager }));
    });
  });
});
