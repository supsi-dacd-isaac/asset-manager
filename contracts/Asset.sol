 pragma solidity ^0.4.24;

import "@aragon/os/contracts/lib/token/ERC20.sol";
import "@aragon/os/contracts/lib/math/SafeMath.sol";

import "./token/MintableToken.sol";
import "./RVT.sol";
import "./RevenueClaiming.sol";
import "./utils/Ownable.sol";

contract Asset is Ownable {
    using SafeMath for uint;

    // Enum definitions

    // Type of the market
    enum State {
                    ACTIVE,
                    FROZEN,
                    INACTIVE
               }

    // Events

    /// Claim executed
    event ClaimExecuted(address indexed claimer, uint amount);

    // Asset features
    struct AssetFeatures {
        // Numerical identifier
        uint id;

        // Asset manager (e.g. the administrator of a self-consumption community)
        address manager;

        // Name
        string name;

        // Description
        string description;

        // State
        State state;

        // List of the share owners
        address[] shareOwners;
    }

    // Mappings

    /// Features of the assets (e.g. name, shares, etc.)
    AssetFeatures private features;

    /// Address related to RevenueClaiming currently activated (i.e. the asset is frozen)
    address activeRevenueClaiming;

    /// Shares token
    SHT sht;

    /// Revenues token
    RVT rvt;

    /// RevenueClaiming list
    address[] revenueClaimings;

    /// Constructor
    /// @param _id asset id
    /// @param _name asset name
    /// @param _description asset description
    /// @param _manager asset manager
    /// @param _rvtAddress RVT address manager
    constructor(uint _id, string _name, string _description, address _manager, address _rvtAddress) public {
        // create the new asset instance
        features.id = _id;
        features.name = _name;
        features.description = _description;
        features.manager = _manager;
        features.state = State.ACTIVE;
        activeRevenueClaiming = address(0);

        // create the token SC for the shares handling and mint them, assigning all to itself
        sht = new SHT(address(this));
        sht.mint(address(this), 1000);

        // define the RVT instance
        rvt = RVT(_rvtAddress);
    }

    /// Set revenue to an asset
    /// @param _amount revenue amount
    function setRevenue(uint _amount) public onlyOwner {
        // check if the asset is active
        require(features.state == State.ACTIVE);

        // freeze the asset
        features.state = State.FROZEN;

        // The manager sends the tokens
        rvt.transferFrom(features.manager, address(this), _amount);

        // Create the RevenueClaiming instance and save the address
        RevenueClaiming revenueClaiming = new RevenueClaiming(_amount);
        activeRevenueClaiming = address(revenueClaiming);
        revenueClaimings.push(address(revenueClaiming));
    }

    /// Defrost a frozen asset
    function defrost() public onlyOwner {
        uint notClaimedRevenues;

        // check if the asset is not active
        require(features.state == State.FROZEN);

        // activate the asset
        features.state = State.ACTIVE;

        // send not claimed revenue tokens to the manager
        notClaimedRevenues = RevenueClaiming(activeRevenueClaiming).getRevenueAmount();
        notClaimedRevenues = notClaimedRevenues.sub(RevenueClaiming(activeRevenueClaiming).getTotalClaimedAmount());

        // The manager sends the tokens
        rvt.transfer(features.manager, notClaimedRevenues);

        // set RevenueClaiming to address 0
        activeRevenueClaiming = address(0);
    }

    /// Activate an inactive asset
    function activate() public onlyOwner {
        // check if the asset is not active
        require(features.state == State.INACTIVE);

        // activate the asset
        features.state = State.ACTIVE;
    }

    /// Deactivate an asset
    function deactivate() public onlyOwner {

        // check if the asset is active
        require(features.state == State.ACTIVE);

        // freeze the asset
        features.state = State.INACTIVE;
    }

    /// Claim the revenue
    function claim() public {
        uint totRevenueAmount;
        uint revenueAmount;

        // check if the asset is frozen
        require(features.state == State.FROZEN);

        // check if the sender owns shares
        require(sht.balanceOf(msg.sender) > 0);

        // check if sender has never requested the claiming for the current revenue
        require(RevenueClaiming(activeRevenueClaiming).checkClaim(msg.sender) == 0);

        // transfer the RVT to msg.sender, according to the shares (asset = balance[asset]*shares[msg.sender]/1000
        totRevenueAmount = RevenueClaiming(activeRevenueClaiming).getRevenueAmount();
        revenueAmount = totRevenueAmount.mul(sht.balanceOf(msg.sender)).div(1000);
        rvt.transfer(msg.sender, revenueAmount);

        // set the revenue claiming as done
        RevenueClaiming(activeRevenueClaiming).setClaimed(msg.sender, revenueAmount);

        emit ClaimExecuted(msg.sender, revenueAmount);
    }

    /// Distribute shares
    /// @param _receivers receivers
    /// @param _amounts share amounts
    function distributeShares(address[] _receivers, uint[] _amounts) public onlyOwner {

        // check if the asset is active
        require(features.state == State.ACTIVE);

        // receivers and amounts arrays must have the same length
        require(_receivers.length == _amounts.length);

        // Check if the asset has sufficient shares to distribute
        uint total = 0;
        for (uint i=0; i<_amounts.length; i++) {
            total = total.add(_amounts[i]);
        }
        require(sht.balanceOf(address(this)) >= total);

        // Transfer shares to the receivers
        for (i=0; i<_amounts.length; i++) {
            // Check if amount is positive and receiver not the 0x0 address
            if((_amounts[i] > 0) && ((_receivers[i] != address(0)))) {
                sht.transfer(_receivers[i], _amounts[i]);
            }
        }
    }

    /// @return the asset status
    function getState() public view returns (State) {
        return features.state;
    }

    /// @return asset name
    function getName() public view returns (string) {
        return features.name;
    }

    /// @return manager address
    function getManager() public view returns (address) {
        return features.manager;
    }

    /// @return asset description
    function getDescription() public view returns (string) {
        return features.description;
    }

    /// @return SHT address
    function getSHTAddress() public view returns (address) {
        return address(sht);
    }

    /// @return RVT address
    function getRVTAddress() public view returns (address) {
        return address(rvt);
    }

    /// @return list of revenueClaimings address
    function getRevenueClaimings() public view returns (address[]) {
        return revenueClaimings;
    }

    /// @return active revenue claiming address
    function getActiveRevenueClaiming() public view returns (address) {
        return activeRevenueClaiming;
    }
}

contract SHT is MintableToken {
    using SafeMath for uint;

    string public name = "Share Token";
    string public symbol = "SHT";
    uint8 public decimals = 0;

    address public asset;

    /// Constructor
    /// @param _asset asset address
    constructor(address _asset) public {
        asset = _asset;
    }

    // ERC20 overrides: the asset has to be active to perform the operations

    function transfer(address _to, uint256 _value) public returns (bool) {
        require(Asset(asset).getState() == Asset.State.ACTIVE);
        return super.transfer(_to, _value);
    }

    function approve(address _spender, uint256 _value) public returns (bool) {
        require(Asset(asset).getState() == Asset.State.ACTIVE);
        return super.approve(_spender, _value);
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool) {
        require(Asset(asset).getState() == Asset.State.ACTIVE);
        return super.transferFrom(_from, _to, _value);
    }

    function increaseApproval(address _spender, uint256 _addedValue) public returns (bool) {
        require(Asset(asset).getState() == Asset.State.ACTIVE);
        return super.increaseApproval(_spender, _addedValue);
    }

    function decreaseApproval(address _spender, uint256 _subtractedValue) public returns (bool) {
        require(Asset(asset).getState() == Asset.State.ACTIVE);
        return super.decreaseApproval(_spender, _subtractedValue);
    }

    /// @return asset address related to SHT
    function getAssetAdddress() public view returns (address) {
        return asset;
    }
}
