pragma solidity ^0.4.24;

import "@aragon/os/contracts/apps/AragonApp.sol";
import "@aragon/os/contracts/lib/math/SafeMath.sol";

import "./Asset.sol";
import "./RVT.sol";
import "./utils/Ownable.sol";

contract AssetManager is AragonApp, Ownable {
    using SafeMath for uint256;

    // Events

    /// Asset created
    event AssetCreated(address indexed entity, uint id);
    /// Asset deactivated
    event AssetDeactivated(address indexed entity, uint id);
    /// Asset activated
    event AssetActivated(address indexed entity, uint id);
    /// Asset frozen
    event AssetFrozen(address indexed entity, uint id);
    /// Asset defrosted
    event AssetDefrosted(address indexed entity, uint id);
    /// Shares distributed
    event SharesDistributed(address indexed entity, uint numReceivers);
    /// Revenue set
    event RevenueSet(address indexed sender, uint assetId, uint amount);

    /// List of the assets
    mapping (uint => address) public assetsList;

    // ACL variables

    /// ACL variable for asset creation
    bytes32 constant public CREATE_ASSET_ROLE = keccak256("CREATE_ASSET_ROLE");
    /// ACL variable for asset deactivation
    bytes32 constant public DEACTIVATE_ASSET_ROLE = keccak256("DEACTIVATE_ASSET_ROLE");
    /// ACL variable for asset activation
    bytes32 constant public ACTIVATE_ASSET_ROLE = keccak256("ACTIVATE_ASSET_ROLE");
    /// ACL variable for shares distribution
    bytes32 constant public DISTRIBUTE_SHARES_ROLE = keccak256("DISTRIBUTE_SHARES_ROLE");

    /// App initialization
    function initialize() public onlyInit {
        initialized();
    }

    /// Create an asset to the list
    /// @param _id asset id
    /// @param _name asset name
    /// @param _description asset description
    /// @param _manager asset manager
    /// @param _rvtAddress RVT address manager
    function createAsset(uint _id, string _name, string _description, address _manager, address _rvtAddress) external auth(CREATE_ASSET_ROLE) {
        // Check if the asset was already created
        require(assetsList[_id] == 0);

        // Add the asset
        Asset asset = new Asset(_id, _name, _description, _manager, _rvtAddress);
        assetsList[_id] = asset;

        emit AssetCreated(msg.sender, _id);
    }

    /// Activate an asset
    /// @param _id asset id
    function activateAsset(uint _id) external auth(ACTIVATE_ASSET_ROLE) {
        Asset(assetsList[_id]).activate();
        emit AssetActivated(msg.sender, _id);
    }

    /// Deactivate an asset
    /// @param _id asset id
    function deactivateAsset(uint _id) external auth(DEACTIVATE_ASSET_ROLE) {
        Asset(assetsList[_id]).deactivate();
        emit AssetDeactivated(msg.sender, _id);
    }

    /// Distribute shares
    /// @param _id asset id
    /// @param _receivers receivers
    /// @param _amounts share amounts
    function distributeShares(uint _id, address[] _receivers, uint[] _amounts) external auth(DISTRIBUTE_SHARES_ROLE) {
        Asset(assetsList[_id]).distributeShares(_receivers, _amounts);
        emit SharesDistributed(msg.sender, _receivers.length);
    }

    /// Set revenue related to an asset
    /// @param _id asset id
    /// @param _amount revenue amount
    function setRevenue(uint _id, uint _amount) external {
        // the sender must be the asset manager
        require(getManager(_id) == msg.sender);

        // set the revenue
        Asset(assetsList[_id]).setRevenue(_amount);

        emit RevenueSet(msg.sender, _id, _amount);
        emit AssetFrozen(msg.sender, _id);
    }

    /// Defrost an asset
    /// @param _id asset id
    function defrostAsset(uint _id) external {
        // the sender must be the asset manager
        require(getManager(_id) == msg.sender);

        Asset(assetsList[_id]).defrost();
        emit AssetDefrosted(msg.sender, _id);
    }

    /// @param _id the id to get
    /// @return SHT address
    function getSHTAddress(uint _id) public view returns (address) {
        return Asset(assetsList[_id]).getSHTAddress();
    }

    /// @param _id the id to get
    /// @return manager address
    function getManager(uint _id) public view returns (address) {
        return Asset(assetsList[_id]).getManager();
    }

    /// @param _id the id to get
    /// @return asset address
    function getAssetAddress(uint _id) public view returns (address) {
        return assetsList[_id];
    }
}
