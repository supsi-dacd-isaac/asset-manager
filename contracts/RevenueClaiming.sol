 pragma solidity ^0.4.24;

import "@aragon/os/contracts/lib/math/SafeMath.sol";

import "./utils/Ownable.sol";

contract RevenueClaiming is Ownable {
    using SafeMath for uint;

    // Variables

    /// Claimed amounts
    mapping (address => uint) public claimedAmounts;
    /// Revenue amount
    uint public revenueAmount;
    /// Claimed amount
    uint public revenueClaimed;
    /// List of the claimers
    address[] public claimers;

    /// Constructor
    /// @param _amount total claimable amount
    constructor(uint _amount) public {
        revenueAmount = _amount;
    }

    /// Set token claiming as done
    /// @param _claimer claimer address
    function setClaimed(address _claimer, uint _amount) public onlyOwner {
        claimedAmounts[_claimer] = _amount;
        revenueClaimed = revenueClaimed.add(_amount);
        claimers.push(_claimer);
    }

    /// Check if the token claiming is allowed
    /// @param _claimer claimer address
    /// @return claiming status (0: not yet claimed | n>0: n tokens were claimed)
    function checkClaim(address _claimer) public view returns(uint) {
        return claimedAmounts[_claimer];
    }

    /// @param _claimer claimer address
    /// @return amount claimed
    function getClaimedAmount(address _claimer) public view returns (uint) {
        return claimedAmounts[_claimer];
    }

    /// @return total revenue amount
    function getRevenueAmount() public view returns (uint) {
        return revenueAmount;
    }

    /// @return claimed revenue amount
    function getTotalClaimedAmount() public view returns (uint) {
        return revenueClaimed;
    }

    /// @return amount claimed
    function getClaimersNumber() public view returns (uint) {
        return claimers.length;
    }

    /// @return claimer address
    function getClaimerAddress(uint idx) public view returns (address) {
        if(idx < claimers.length) {
            return claimers[idx];
        }
        else {
            return address(0);
        }
    }
}
