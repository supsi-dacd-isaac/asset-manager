pragma solidity ^0.4.24;

import "@aragon/os/contracts/lib/math/SafeMath.sol";
import "./token/MintableToken.sol";

contract RVT is MintableToken {
    using SafeMath for uint;

    string public name = "Revenues Token";
    string public symbol = "RVT";
    uint8 public decimals = 18;
}
