
# asset-manager

The basic idea of the smart contracts is to manage generic assets using the platform provided by [Aragon](https://aragon.org/).

The documentation is available on [Read The Docs](https://asset-manager.readthedocs.io).

# Acknowledgements
The authors would like to thank the Swiss Federal Office of Energy (SFOE) and the Swiss Competence Center for Energy Research - Future Swiss Electrical Infrastructure (SCCER-FURIES), for their financial and technical support to this research work.
